<?php namespace Codenamegary\L4blog\Migrations;

use \Illuminate\Database\Migrations\Migration;
use \Illuminate\Support\Facades\Config;

class Base extends Migration {

    protected $prefix;
    protected $usersTable;
    protected $usersPK;
    protected $app;

    public function __construct()
    {
        $this->prefix = Config::get('l4blog::database.prefix');
        $this->usersTable = Config::get('l4blog::database.usersTable');
        $this->usersPK = Config::get('l4blog::database.usersPK');
    }

    // Returns the suffixed version of $name
    protected function table( $name )
    {
        return $this->prefix . $name ;
    }

}