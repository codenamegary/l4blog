<?php namespace Codenamegary\L4blog\Controllers;

class Home extends Base {

    public function getIndex()
    {
        return $this->respond('l4blog::home');
    }

}