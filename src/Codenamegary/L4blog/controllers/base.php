<?php namespace Codenamegary\L4blog\Controllers;

use Illuminate\Routing\Controllers\Controller;
use Illuminate\Support\Facades\Config as Config;
use Illuminate\Support\Facades\View as View;

class Base extends Controller {

    protected $layoutName;
    protected $sectionNames;
    protected $sectionContent;

    public function __construct()
    {
        $this->layoutName = Config::get('l4blog::views.layout');
        $this->sectionNames['content'] = Config::get('l4blog::views.sections.content','');
    }

    public function respond( $view )
    {
        return View::make( $view )
            ->with( 'layoutName', $this->layoutName )
            ->with( 'sectionNames', $this->sectionNames );
    }

}