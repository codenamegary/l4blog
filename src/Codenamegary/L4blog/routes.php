<?php

$routes = Config::get('l4blog::routing');

// Article view routes
//Route::get( $routes['articles'],  );

Route::get( $routes['prefix'], array(
    'as'        => 'codenamegary::l4blog::home',
    'uses'      => '\Codenamegary\L4blog\Controllers\Home@getIndex',
));