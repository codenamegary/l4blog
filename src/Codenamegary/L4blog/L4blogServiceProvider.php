<?php namespace Codenamegary\L4blog;

use Illuminate\Support\ServiceProvider;
use Codenamegary\L4blog\Migrations\Base;

class L4blogServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
        $this->package('codenamegary/l4blog');
        include __DIR__ . '/routes.php';
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		//
        $this->app['config']->package('codenamegary/l4blog', __DIR__.'/../../config');
        //die(var_dump($this->app['config']->get('l4blog::routing')));
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}