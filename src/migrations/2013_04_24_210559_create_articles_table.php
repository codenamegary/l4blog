<?php

use Codenamegary\L4blog\Migrations\Base;

class CreateArticlesTable extends Base {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::create( $this->table('articles'), function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title',255);
            $table->dateTime('publish_start')->nullable()->default(null);
            $table->dateTime('publish_stop')->nullable()->default(null);
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::drop( $this->table('articles') );
	}

}