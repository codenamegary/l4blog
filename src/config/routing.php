<?php

return array(

    // The blog-prefix is inserted before every route that
    // the l4blog package handles.
    'prefix'            => 'blog',

    // Routes for individual articles
    'articles'          => array(
        '{articleSlug}',
        '{date}/{articleSlug}',
    ),
    'tags'              => array(
        '{tagSlug}',
        '{tagSlug}/{articleSlug}',
    ),
    'authors'           => array(
        '{authorSlug}',
        '{authorSlug}/{tagSlug}',
        '{authorSlug}/{tagSlug}/{articleSlug}',
        '{authorSlug}/{articleSlug}',
    ),

);