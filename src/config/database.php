<?php

return array(

    // Prefix will be applied to every table name used by l4blog
    'prefix'        => 'l4blog_',

    // The table that stores users, l4blog will FK to this
    // table and provide extended author info. Set to "false"
    // if no users table is present or you do not wish to
    // map authors to user accounts in your app.
    'usersTable'    => 'users',
    'usersPK'       => 'id',
);